package repository;

import config.DBConnection;
import pojo.User;
import repository.specifications.QueryInfo;
import repository.specifications.Specification;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserRepositoryImpl implements UserRepository<User> {

    public User create(User user) {
        Connection conn = null;
        PreparedStatement statement = null;

        String sql = "INSERT INTO user VALUE (?, ?, ?, ?)";

        try {
            conn = DBConnection.getConnection();
            statement = conn.prepareStatement(sql);
            statement.setInt(1, user.getId());
            statement.setString(2, user.getUsername());
            statement.setString(3, user.getPassword());
            statement.setInt(4, user.getAge());
            int i = statement.executeUpdate();
            System.out.println("Affected rows" + i);
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(UserRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(UserRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return user;
    }

    public User update(User account) {
        return null;
    }

    public User delete(User account) {
        return null;
    }

    public List<User> query(Specification<User> specification) {
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        List<User> returnValue = new ArrayList<User>();
        try {
            conn = DBConnection.getConnection();

            QueryInfo queryInfo = specification.toQueryInfo();
            String sql = queryInfo.getSql();
            Map<Integer, Object> placeholders = queryInfo.getPlaceholders();

            statement = conn.prepareStatement(sql);

            for (Map.Entry<Integer, Object> entry : placeholders.entrySet()) {
                statement.setObject(entry.getKey(), entry.getValue());
            }
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                int userId = resultSet.getInt(1);
                String username = resultSet.getString(2);
                String password = resultSet.getString(3);
                int age = resultSet.getInt(4);
                returnValue.add(new User(userId, username, password, age));
            }
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(UserRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(UserRepositoryImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return returnValue;
    }
}
