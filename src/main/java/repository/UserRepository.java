package repository;

import repository.specifications.Specification;

import java.util.List;

public interface UserRepository<T> {

    T create(T user);

    T update(T account);

    T delete(T account); // Think it as replace for set

    List<T> query(Specification<T> specification);
}
