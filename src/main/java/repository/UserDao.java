package repository;

import pojo.User;

public interface UserDao<T> {

    User get(int id);

    void create(T user);

    void update(T user);

    void delete(int id);

    User getByAge(int age);

    User getByUsernameAndPassword(String username, String password);
}
