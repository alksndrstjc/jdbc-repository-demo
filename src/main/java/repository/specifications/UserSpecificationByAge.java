package repository.specifications;

import pojo.User;

import java.util.HashMap;
import java.util.Map;

public class UserSpecificationByAge implements Specification<User> {

    private int age;

    public UserSpecificationByAge(int age) {
        this.age = age;
    }

    @Override
    public QueryInfo toQueryInfo() {
        QueryInfo info = new QueryInfo();
        //builder can be used here
        String sql = "SELECT * FROM user WHERE age = ?";
        Map<Integer, Object> placeholders = new HashMap<>();
        placeholders.put(1, age);
        info.setSql(sql);
        info.setPlaceholders(placeholders);
        return info;
    }
}
