package repository.specifications;

public interface Specification<T> {

    QueryInfo toQueryInfo();
}
