package repository.specifications;

import pojo.User;

import java.util.HashMap;
import java.util.Map;

public class UserSpecificationById implements Specification<User> {

    private int id;

    public UserSpecificationById(int id) {
        this.id = id;
    }

    public QueryInfo toQueryInfo() {
        QueryInfo info = new QueryInfo();
        //builder can be used here
        String sql = "SELECT * FROM user WHERE id = ?";
        Map<Integer, Object> placeholders = new HashMap<>();
        placeholders.put(1, id);
        info.setSql(sql);
        info.setPlaceholders(placeholders);
        return info;
    }
}
