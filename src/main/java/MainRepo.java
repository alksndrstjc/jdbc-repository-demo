import pojo.User;
import repository.UserDao;
import repository.UserDaoImpl;
import repository.UserRepositoryImpl;
import services.UserService;
import services.UserServiceImpl;

import java.util.List;

public class MainRepo {

    public static void main(String[] args) {

        /*
        todo:
        4. structure - why use patterns like DAO, repository.
        5. an example of Service -> Repository -> DB
        */

        MainJDBC.createUserTable();

        User user1 = new User(1, "aleks", "aleks", 28);
        User user2 = new User(2, "aleks", "aleks", 28);

        //Why these patterns?

        //It separates the domain logic that use it from any particular persistence mechanism or APIs

        //The interface methods signature are independent of the content of the User class. When you add a telephone number field to the User, you don’t need to change the UserDao interface nor its callers’

        //Why repository?

        //daos become bloated and tightly coupled, testing becomes harder
        // with each new dao method im coupling User with it's Dao object more making changes to the interfaces required
        UserDao<User> userDao = new UserDaoImpl();

        //new UserRepositroy -> mysql, postgresql, mongo etc. this decoupling allows us to switch to different dbs without any changes to code
        UserService<User> userService = new UserServiceImpl(new UserRepositoryImpl());

        userService.create(user1);
        userService.create(user2);
        User returnedUser1 = userService.findUserById(1);
        List<User> alekses = userService.findAllUsersWithUsername("aleks");

        List<User> usersz = userService.findAllUsersWithAge(28);

    }
}
