package services;

import pojo.User;

import java.util.List;

public interface UserService<T> extends Service<User> {

    User findUserById(int id);

    List<User> findAllUsersWithUsername(String username);

    List<User> findAllUsersWithAge(int age);
}
