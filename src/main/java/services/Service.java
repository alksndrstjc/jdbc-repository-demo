package services;

import pojo.Base;

public interface Service<T extends Base> {

    T create(T entity);

    T update(T entity);

    T delete(T entity);
    
}
