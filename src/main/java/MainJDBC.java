import config.DBConnection;
import pojo.User;
import repository.SqlBuilder;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainJDBC {

    public static void main(String[] args) {

        /*
        todo:
        1. what is jdbc - specification for doing db operations
        2. isolated examples of ddl and 2 dml - creating user table, inserting a record an fetching the record by id
        3. some things to pay attention - use PreparedStatement sql injection, use Base as an abstraction, enforce type safety with framework or try to create your own query builder
         */

        //1. jdbc usage
        createUserTable();

        insertUserInTheTable();

        // mysql injection, use prepared statement instead
        String name = "peter";
        String nameWithMysqlInjection = " '); DROP TABLE user;--";
        insertAnotherWithNameMysqlInjection(nameWithMysqlInjection);

        // example with prepared statement
        User user = fetchUser(1);
        System.out.println(user);

        //anatomy of sql statements and query builder example
        // SELECT FROM user WHERE id = 1;
        // jooq
        String sql = new SqlBuilder("")
                .select("user")
                .where()
                .fields("id", "1").getSql();
        User user2 = fetchUserUsingBuilder(1);
        System.out.println(user2);

    }

    public static void createUserTable() {
        String userTableCreation = "CREATE TABLE user(" +
                "id INT(11) AUTO_INCREMENT, " +
                "username VARCHAR(50) NOT NULL, " +
                "password VARCHAR(50) NOT NULL," +
                "age INT(11) NOT NULL," +
                "PRIMARY KEY(id)) ENGINE=InnoDB DEFAULT CHARSET=utf8";

        Connection conn = null;
        Statement statement = null;
        try {
            conn = DBConnection.getConnection();
            statement = conn.createStatement();
            statement.execute(userTableCreation);
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MainJDBC.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MainJDBC.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private static void insertUserInTheTable() {
        String sql = "INSERT INTO user VALUE ('1', 'aleks', 'pass', '28')";

        Connection conn = null;
        Statement statement = null;
        try {
            conn = DBConnection.getConnection();
            statement = conn.createStatement();
            int i = statement.executeUpdate(sql);
            System.out.println("Number of rows mofidied: " + i);
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MainJDBC.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MainJDBC.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private static void insertAnotherWithNameMysqlInjection(String name) {
        Connection conn = null;
        Statement statement = null;
        try {
            String insertsql = "INSERT INTO user VALUE (' " + name + "')";
            conn = DBConnection.getConnection();
            conn.createStatement().executeUpdate(insertsql);
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MainJDBC.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MainJDBC.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private static User fetchUser(int id) {
        String sql = "SELECT * FROM user WHERE id = ?";

        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        User returnValue = null;
        try {
            conn = DBConnection.getConnection();
            statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                int userId = resultSet.getInt(1);
                String username = resultSet.getString(2);
                String password = resultSet.getString(3);
                int age = resultSet.getInt(4);

                returnValue = new User(userId, username, password, age);
            }
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MainJDBC.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MainJDBC.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return returnValue;
    }

    private static User fetchUserUsingBuilder(int id) {

        String sql = new SqlBuilder("").select("user").where()
                .fields("id", "?").getSql();

        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        User returnValue = null;
        try {
            conn = DBConnection.getConnection();
            statement = conn.prepareStatement(sql);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                int userId = resultSet.getInt(1);
                String username = resultSet.getString(2);
                String password = resultSet.getString(3);
                int age = resultSet.getInt(4);

                returnValue = new User(userId, username, password, age);
            }
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MainJDBC.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(MainJDBC.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return returnValue;
    }

}

